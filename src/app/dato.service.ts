import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './Usuario';

@Injectable({
  providedIn: 'root'
})
export class DatoService {
	//url = 'https://jsonplaceholder.typicode.com/posts';
  //url = 'http://localhost:3000/api/pruebas';
  url = 'http://localhost:3000/api/crear';
  urllista = 'http://localhost:3000/api/listar';
  urllogin = 'http://localhost:3000/api/login';
  constructor(private http: HttpClient ) { 
    
  }
  obtenerDatos(){
    return this.http.get<Usuario[]>(this.urllista);
  }
  enviarDatos(user: Usuario){
  	return this.http.post<any>(this.url, user);
  }
  login(user: Usuario){
    return this.http.post<any>(this.urllogin, user);
  }
}
//https://www.youtube.com/watch?v=jwA-9XXybdM&t=8s