export class Usuario {

	public nombre: string;
    public correo: string;
    public dni: string;
    public usu: string;
    public pass: string;

	constructor(nombre?: string, correo?: string, dni?: string, usu?: string, pass?: string){
		this.nombre= nombre,
		this.correo= correo,
		this.dni= dni,
		this.usu= usu,
		this.pass = pass
	}
}
