import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrausuarioComponent } from './frausuario.component';

describe('FrausuarioComponent', () => {
  let component: FrausuarioComponent;
  let fixture: ComponentFixture<FrausuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrausuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrausuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
