import { Component, OnInit } from '@angular/core';
import { DatoService } from '../dato.service';
import { Usuario } from '../Usuario';

@Component({
  selector: 'app-iniciarsesion',
  templateUrl: './iniciarsesion.component.html',
  styleUrls: ['./iniciarsesion.component.sass']
})
export class IniciarsesionComponent implements OnInit {
	
	usus = new Usuario();

  constructor(private ds: DatoService){}

  ngOnInit() {
  }

   submit1(){
  this.ds.login(this.usus)
  .subscribe(
    data => console.log(':)', data),
    error => console.log('Error', error)
    );
}
}
