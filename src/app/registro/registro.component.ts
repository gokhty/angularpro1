import { Component, OnInit } from '@angular/core';

//otros metodos
import { DatoService } from '../dato.service';
import { Usuario } from '../Usuario';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.sass']
})
export class RegistroComponent implements OnInit {
	usus = new Usuario();
  constructor(private ds: DatoService){
    this.mostrar();
  }

  ngOnInit() {
  }

  submit1(){
	//this.txtNum = "hola";
	//alert(this.txtNum);
  //console.log(this.usus);
  this.ds.enviarDatos(this.usus)
  .subscribe(
    data => console.log(':)', data),
    error => console.log('Error', error)
    );
    this.mostrar();
  
}
arreglo = [];
mostrar(){
    this.ds.obtenerDatos()
  .subscribe(dato =>{
      this.arreglo = dato;
    });
   }

}
