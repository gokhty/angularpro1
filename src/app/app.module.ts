import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { MatInputModule, MatButtonModule, MatSelectModule, MatIconModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// 1http
import { HttpClientModule } from '@angular/common/http';
//2http
import { DatoService } from './dato.service';
import { FrausuarioComponent } from './frausuario/frausuario.component';
import { IniciarsesionComponent } from './iniciarsesion/iniciarsesion.component';
import { RegistroComponent } from './registro/registro.component';

import { RouterModule, Route } from '@angular/router';

const routes: Route[] = [
  {path: 'iniciarsesion', component: IniciarsesionComponent},
  {path: 'registro', component: RegistroComponent}
  ];

@NgModule({
  declarations: [
    AppComponent,
    FrausuarioComponent,
    IniciarsesionComponent,
    RegistroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule, 
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
	 HttpClientModule,
   RouterModule.forRoot(routes),
  ],
  providers: [DatoService],//3http
  bootstrap: [AppComponent]
})
export class AppModule { }
